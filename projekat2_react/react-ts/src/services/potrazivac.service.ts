const url = 'http://localhost:3001/potrazivac'
const url2='http://localhost:3001/radnik/?email='

export function uzmiPotrazivace(){
    return fetch(url)
        .then(response => response.json())
        .catch(error => console.error(error));
}

export function uzmiRadnika(email: string){
    return fetch(url2+email)
        .then(response => response.json())
        .then(radnik=>radnik[0])
        .catch(error => console.error(error));
}