import React, { Component } from 'react';
import './App.css';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { rootReducer } from './store';
import createSagaMiddleware from '@redux-saga/core';
import { rootSaga } from './store/sagas';
import { fetchPotrazivac } from './store/actions/actions';
import PotrazivacList from './components/PotrazivacList';

const sagaMiddleware = createSagaMiddleware();

const potrazivacStore = createStore(rootReducer, applyMiddleware(sagaMiddleware));
sagaMiddleware.run(rootSaga);
potrazivacStore.dispatch(fetchPotrazivac());

class App extends Component {
  render() {
    return (
    <Provider store = {potrazivacStore}>
          <PotrazivacList></PotrazivacList>          
    </Provider>
    );
  }
}

export default App;
