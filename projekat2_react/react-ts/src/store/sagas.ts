import { all, takeEvery, put } from 'redux-saga/effects';
import { FETCH_POTRAZIVAC, addPotrazivac, FETCH_RADNIKA, dodajRadnika, FetchRadnika } from './actions/actions';
import { uzmiPotrazivace, uzmiRadnika } from '../services/potrazivac.service';

function* fetchPotrazivac(){
    const potrazivaci = yield uzmiPotrazivace();
    yield put(addPotrazivac(potrazivaci));
}

function* sagaFetchRadnika(action: FetchRadnika){
    const radnik1 = yield uzmiRadnika(action.email);
    yield put(dodajRadnika(radnik1,action.password));
}

export function* rootSaga(){
    yield all([
        takeEvery(FETCH_POTRAZIVAC, fetchPotrazivac),
        takeEvery(FETCH_RADNIKA, sagaFetchRadnika)
    ])
}