import { Radnik } from "../../components/models/Radnik";
import { Action } from "redux";
import { ADD_RADNIK,  DodajRadnika, VRATI_OMILJENOG, VratiOmiljenog } from "../actions/actions";

const initialState: Radnik = {
    ime: "",
    prezime: "",
    email: "",
    sifra: "",
    godine: 0,
    telefon: "",
    mojiPotrazivaci: []
}

export function radnikReducer(state: Radnik = initialState,action: Action){
    switch(action.type)
    {
        case ADD_RADNIK:{
            const {radnik} = action as DodajRadnika;
            const {password}=action as DodajRadnika;
            if(radnik.sifra===password)
            {
                state=radnik;
            }
            else
            {
                alert("Niste uneli korektan password! Pokusajte ponovo.");
            }
            return state;
        }
        case VRATI_OMILJENOG:{
           var {omiljeni} = action as VratiOmiljenog;
           var niz=state.mojiPotrazivaci;
           console.log(niz);
           niz.push(omiljeni);
           state.mojiPotrazivaci=niz.slice(0,2);
           console.log(state);
           return state;
        }        
        default: return state;
    }
}