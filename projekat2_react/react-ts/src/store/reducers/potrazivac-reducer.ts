import {Potrazivac} from '../../components/models/Potrazivac';
import { Action } from "redux";
import { ADD_POTRAZIVAC, AddPotrazivac,DELETE_POTRAZIVAC, DeletePotrazivac,
    IZBACI_BEZ_PREVOZA_POTRAZIVACE, SORTIRAJ, Sortiraj, POVECAJ_BROJ_RADNIKA, PovecajBrojRadnika } from "../actions/actions";

const initialState: Potrazivac[] = [];

export function potrazivacReducer(state: Potrazivac[] = initialState, action: Action)
{
    switch(action.type)
    {
        case DELETE_POTRAZIVAC: {
            const {brojPotrazivaca} = action as DeletePotrazivac;
            return state.filter((potrazivac: Potrazivac) => potrazivac.brojTelefona !== brojPotrazivaca);
        }

        case ADD_POTRAZIVAC: {
            const {potrazivaci} = action as AddPotrazivac;
            return [...state, ...potrazivaci];
        }

        case IZBACI_BEZ_PREVOZA_POTRAZIVACE: {
            return state.filter((potrazivac: Potrazivac)=> potrazivac.obezbedjeniPrevoz===true)
        }

        case SORTIRAJ: {
            var {atributZaSortiranje} = action as Sortiraj;
            if(atributZaSortiranje==="Ceni"){
                var niz= state.sort((a, b) => (a.cena < b.cena) ? 1 : (a.cena === b.cena) ? ((a.level < b.level) ? 1 : -1) : -1 );
                state=niz.slice(0,10);
            }
            if(atributZaSortiranje==="Poenima")
            {
                var niz1= state.sort((a, b) => (a.poeni < b.poeni) ? 1 : (a.poeni === b.poeni) ? ((a.level < b.level) ? 1 : -1) : -1 );
                state=niz1.slice(0,10);
            }
            return state;
        }

        case POVECAJ_BROJ_RADNIKA: {
            var {id} = action as PovecajBrojRadnika;
            state.forEach((potrazivac: Potrazivac)=>{
                if(potrazivac.brojTelefona===id)
                    potrazivac.trenutiBrojRadika++;
            });
            
            var niz3=state.filter((potrazivac: Potrazivac)=> potrazivac.trenutiBrojRadika<potrazivac.neophodanBrojRadnika);
            state=niz3;

            return state;
        }

        default: return state;
    }
}