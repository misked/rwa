import { combineReducers } from "redux";
import { potrazivacReducer } from "./reducers/potrazivac-reducer";
import { Potrazivac } from "../components/models/Potrazivac";
import { Radnik } from "../components/models/Radnik";
import { radnikReducer } from "./reducers/radnik-reducer";


export interface AppState {
    potrazivaci: Potrazivac[],
    radnik: Radnik
}

export const rootReducer = combineReducers({
    potrazivaci: potrazivacReducer,
    radnik: radnikReducer
})