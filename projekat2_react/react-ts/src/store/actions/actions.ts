import { Action } from "redux";
import {Potrazivac} from "../../components/models/Potrazivac";
import {Radnik} from "../../components/models/Radnik";


export const FETCH_POTRAZIVAC = "FETCH POTRAZIVAC";
export const ADD_POTRAZIVAC = "ADD POTRAZIVAC";
export const DELETE_POTRAZIVAC = "DELETE POTRAZIVAC";
export const ADD_RADNIK = "ADD RADNIK";
export const FETCH_RADNIKA = "FETCH RADNIKA";
export const IZBACI_BEZ_PREVOZA_POTRAZIVACE="IZBACI BEZ PREVOZA POTRAZIVACE";
export const SORTIRAJ= "SORTIRAJ";
export const POVECAJ_BROJ_RADNIKA="POVECAJ BROJ RADNIKA";
export const VRATI_OMILJENOG="VRATI OMILJENOG";


export interface AddPotrazivac extends Action{
    potrazivaci: Potrazivac[]
}
export function addPotrazivac(potrazivaci: Potrazivac[]): AddPotrazivac {
    return {
        type: ADD_POTRAZIVAC,
        potrazivaci: potrazivaci
    }
}


export interface FetchPotrazivac extends Action {
}
export function fetchPotrazivac() : FetchPotrazivac{
    return {
        type: FETCH_POTRAZIVAC
    };
}


export interface DeletePotrazivac extends Action {
    brojPotrazivaca: string;
}
export function deletePotrazivac(brojPotrazivaca: string): DeletePotrazivac{
    return {
        type: DELETE_POTRAZIVAC,
        brojPotrazivaca: brojPotrazivaca
    }
}


export interface DodajRadnika extends Action {
    radnik: Radnik,
    password: string
}
export function dodajRadnika(radnik: Radnik, password:string): DodajRadnika{
    return{
        type: ADD_RADNIK,
        radnik: radnik,
        password:password
    }
}


export interface FetchRadnika extends Action {
    email: string,
    password: string
}
export function fetchRadnika(email: string,password:string) : FetchRadnika{
    return {
        type: FETCH_RADNIKA,
        email: email,
        password:password
    };
}


export interface IzbaciBezPrevozaPotrazivace extends Action{
}
export function izbaciBezPrevozaPotrazivace(): IzbaciBezPrevozaPotrazivace{
    return{
        type: IZBACI_BEZ_PREVOZA_POTRAZIVACE
    }
}


export interface Sortiraj extends Action{
    atributZaSortiranje: String
}
export function sortiraj(atributZaSortiranje: String): Sortiraj{
    return{
        type: SORTIRAJ,
        atributZaSortiranje: atributZaSortiranje
    }
}


export interface PovecajBrojRadnika extends Action{
    id: string
}
export function povecajBrojRadnika(id: string): PovecajBrojRadnika{
    return{
        type: POVECAJ_BROJ_RADNIKA,
        id: id
    }
}


export interface VratiOmiljenog extends Action{
    omiljeni: Potrazivac
}
export function vratiOmiljenog(omiljeni: Potrazivac):VratiOmiljenog{
    return{
        type: VRATI_OMILJENOG,
        omiljeni: omiljeni
    }
}