export interface Potrazivac {
    ime: string;
    prezime: string;
    opisPosla: string;
    cena: number,
    brojTelefona: string,
    neophodanBrojRadnika: number,
    trenutiBrojRadika: number,
    obezbedjeniPrevoz: boolean,
    grupnoIme: string;
    grad: string;
    nagrade: number;
    brojRadova: number;
    prijatelji?: number;
    poeni: number;
    level: number;
}