import { Potrazivac } from "./Potrazivac";

export interface Radnik {
    ime: string;
    prezime: string;
    email: string;
    sifra: string;
    godine?: number;
    telefon: string;
    mojiPotrazivaci: Potrazivac[];
}