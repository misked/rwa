import React, { Component, Dispatch } from "react";
import { Radnik } from "../components/models/Radnik";
import { AppState } from "../store";
import { connect } from "react-redux";
import { Action } from "redux";
import { fetchRadnika } from "../store/actions/actions";
import { Potrazivac } from "./models/Potrazivac";

interface Props {
    radnikIzReducera: Radnik;
    dodajRadnikaKlikom: Function;
}

interface State {
    username: string;
    password: string;
}

class Radnici extends Component<Props, State> {
    constructor(props: Props)
    {
        super(props);
        this.state={
            username: '',
            password: ''
        }
        this.onSubmit=this.onSubmit.bind(this);
    }

    onSubmit = (e:any) => {
        e.preventDefault();
        var emailPokusaj=this.state.username;
        var password=this.state.password;
        this.props.dodajRadnikaKlikom(emailPokusaj,password);
    }

    logout(){
        document.location.reload();
    }

    render() {
        if (!this.props.radnikIzReducera.ime) {
            return (
                <div>
                    <form onSubmit={this.onSubmit} className="strana levaStrana">
                        <div>
                        <h1>
                            Log In
                        </h1>
                        <div>
                            Korisnicko ime <br />
                            <input name="username" value={this.state.username} onChange={e => this.setState({ username: e.target.value })} />
                        </div>
                        <div>
                            Lozinka <br />
                            <input name="password" value={this.state.password} type="password" onChange={e => this.setState({ password: e.target.value })} />
                        </div>
                        <br />
                        <button>Prijavi se</button>
                        </div>
                    </form>
                </div>);
        }
        return (<div className="strana levaStrana">
            <div className="upit">
                <p className="tekst">Ime radnika je: {this.props.radnikIzReducera.ime} </p>
            </div>
            <div className="upit">
                <p className="tekst">Prezime radnika je: {this.props.radnikIzReducera.prezime} </p>
            </div>
            <div className="upit">
                <p className="tekst">Broj telefona: {this.props.radnikIzReducera.telefon}</p>
            </div>
            <div className="upit">
                <p className="tekst">E-mail: {this.props.radnikIzReducera.email}</p>
            </div>
            <div className="upit">
                <p className="tekst">Broj godina: {this.props.radnikIzReducera.godine}</p>
            </div>
            <div className="upit">
                <p className="tekst">Moji omiljeni potrazivaci: </p>
            </div>
            <div>
                {this.props.radnikIzReducera.mojiPotrazivaci.map((potrazivac: Potrazivac) => (
                    <p key={potrazivac.brojTelefona}>{potrazivac.ime} {potrazivac.prezime},{potrazivac.brojTelefona}</p>
                ))}
            </div>
            <div>
                <button className="upit" onClick={this.logout}>Odjavi se</button>
            </div>
        </div>)
    }
}

function mapDispatchToProps(dispatch: Dispatch<Action>){
    return {
        dodajRadnikaKlikom: (email:string,password:string) => dispatch(fetchRadnika(email,password))
    }
}

function mapStateToProps(state: AppState) {
    return {
        radnikIzReducera: state.radnik,
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Radnici);