import { Professor } from '../models/professor.models'
import * as ProfessorActions from '../actions/myActions'

export interface LoginState {
    professorWhoLoggedIn: Professor,
    loading: boolean,
    error: Error
}

const initialState: LoginState = {
    professorWhoLoggedIn: null,
    loading: false,
    error: undefined
}

export function loginReducer(state: LoginState = initialState, action: ProfessorActions.Actions) {
    switch (action.type) {
        case ProfessorActions.LOGIN_PROFESSOR:
            return { ...state, loading: true }
        case ProfessorActions.LOGIN_PROFESSOR_SUCCESS:
            {
                if (action.payload[0].password === action.password) {
                    return { ...state, professorWhoLoggedIn: action.payload[0], loading: false }
                }
                else {
                    alert("Niste uneli korektno sifru!");
                    return state;
                }
            }
        case ProfessorActions.LOGIN_PROFESSOR_FAILURE:
            return { ...state, error: action.payload, loading: false }
        default:
            return state;
    }
}