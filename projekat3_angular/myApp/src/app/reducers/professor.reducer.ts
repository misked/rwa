import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { Professor } from '../models/professor.models';
import * as ProfessorActions from '../actions/myActions';

export interface ProfessorState extends EntityState<Professor> {
    selectedProfessorId: number | null;
}

const professorAdapter = createEntityAdapter<Professor>({
    selectId: (professor : Professor) => professor.id
});

const initialStateProfessor: ProfessorState = professorAdapter.getInitialState({
    selectedProfessorId: null
});

export function professorReducer(state: ProfessorState = initialStateProfessor, action: ProfessorActions.Actions) {
    switch (action.type) {
        case ProfessorActions.LOAD_PROFESSOR:
            return { ...state, loading: true }
        case ProfessorActions.LOAD_PROFESSOR_SUCCESS:
            return professorAdapter.addAll(action.payload,state);
        case ProfessorActions.LOAD_PROFESSOR_FAILURE:
            return { ...state, error: action.payload, loading: false }
        case ProfessorActions.DELETE_PROFESSOR:
            return { ...state, loading: true }
        case ProfessorActions.DELETE_PROFESSOR_SUCCESS:
            return professorAdapter.removeOne(action.payload, state);
        case ProfessorActions.DELETE_PROFESSOR_FAILURE:
            return { ...state, error: action.payload, loading: false }
        default:
            return state;
    }
}

export const selectProfessorState = createFeatureSelector<ProfessorState>('proffesorsFromEntity');

export const { selectAll: selectAllProfessors, selectIds } = professorAdapter.getSelectors(selectProfessorState);

export const getSelectedProfessor = createSelector (
    selectProfessorState,
    (state) => {
        return state.entities[state.selectedProfessorId];
    }
)