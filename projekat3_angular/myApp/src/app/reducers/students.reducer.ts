import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { Student } from '../models/student.models'
import * as StudentActions from '../actions/myActions'


export interface StudentState extends EntityState<Student> {
    selectedStudentId: number | null;
}

const studentAdapter = createEntityAdapter<Student>({
    selectId: (student: Student) => student.id
});

const initialStateStudent: StudentState = studentAdapter.getInitialState({
    selectedStudentId: null
});

export function studentReducer(state: StudentState = initialStateStudent, action: StudentActions.Actions) {
    switch (action.type) {
        case StudentActions.LOAD_STUDENT:
            return { ...state, loading: true }
        case StudentActions.LOAD_STUDENT_SUCCESS:
            return studentAdapter.addAll(action.payload,state);
        case StudentActions.LOAD_STUDENT_FAILURE:
            return { ...state, error: action.payload, loading: false }
        case StudentActions.ADD_STUDENT:
            return { ...state, loading: true }
        case StudentActions.ADD_STUDENT_SUCCESS:
            return studentAdapter.addOne(action.payload,state);
        case StudentActions.ADD_STUDENT_FAILURE:
            return { ...state, error: action.payload, loading: false }
        case StudentActions.DELETE_STUDENT:
            return { ...state, loading: true }
        case StudentActions.DELETE_STUDENT_SUCCESS:
            return studentAdapter.removeOne(action.payload,state);
        case StudentActions.DELETE_STUDENT_FAILURE:
            return { ...state, error: action.payload, loading: false }
        case StudentActions.IMPROVE_STUDENT:
            return { ...state, loading: true }
        case StudentActions.IMPROVE_STUDENT_SUCCESS:
            return studentAdapter.updateOne({
                id: action.payload.id,
                changes: action.payload
              }, state);
        case StudentActions.IMPROVE_STUDENT_FAILURE:
            return { ...state, error: action.payload, loading: false }
        default:
            return state;
    }
}


export const selectStudentState = createFeatureSelector<StudentState>('studentsFromEntity');

export const { selectAll: selectAllStudents, selectIds } = studentAdapter.getSelectors(selectStudentState);

export const getSelectedStudent = createSelector (
    selectStudentState,
    (state) => {
        return state.entities[state.selectedStudentId];
    }
)