import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Student } from '../../models/student.models';
import { AppState } from '../../app.state';
import { Professor } from '../../models/professor.models';
import { v4 as uuid } from 'uuid';
import * as MyActions from '../../actions/myActions';
import { selectAllProfessors } from '../../reducers/professor.reducer';
import { selectAllStudents } from '../../reducers/students.reducer';

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})

export class StudentsComponent implements OnInit {
  Students: Student[];
  Professors: Professor[];
  addButton: Boolean;
  editButton: Boolean;
  StudentWhoIsAdded: Student = {
    id: '',
    name: '',
    surname: '',
    yearOfBirthday: 0,
    birthday: '',
    gender: 'M',
    firefighter: false,
    description: '',
    details: '',
    physicalReadiness: 0,
    behavior: 0,
    responsibility: 0,
    knowledge: 0,
    status: 'student',
  }

  constructor(private store: Store<AppState>) {
    this.addButton = false;
    this.editButton = false;
  }

  ngOnInit() {
    this.getProfessors();
    this.getStudents();
  }

  getStudents(): void {
    this.store.select(selectAllStudents).subscribe(
      students => {
        this.Students=students;
      }
    );
  }

  getProfessors(): void {
    this.store.select(selectAllProfessors).subscribe(
      professors => {
        this.Professors=professors;
      }
    );
  }

  IWantToAdd() {
    this.StudentWhoIsAdded.name = '';
    this.StudentWhoIsAdded.surname = '';
    this.StudentWhoIsAdded.yearOfBirthday = 0;
    this.StudentWhoIsAdded.gender = '';
    this.StudentWhoIsAdded.description = '';
    this.StudentWhoIsAdded.details = '';
    this.StudentWhoIsAdded.physicalReadiness = 0;
    this.StudentWhoIsAdded.behavior = 0;
    this.StudentWhoIsAdded.responsibility = 0;
    this.StudentWhoIsAdded.knowledge = 0;
    this.StudentWhoIsAdded.firefighter = false;
    this.addButton = true;
    this.editButton = false;
  }

  IWantToEdit() {
    this.editButton = true;
    this.addButton = false;
  }

  AddStudent(name, surname, yearOfBirthday, gender, description, details, physicalReadiness, behavior, responsibility, knowledge) {
    this.StudentWhoIsAdded.id = uuid();
    this.StudentWhoIsAdded.name = name;
    this.StudentWhoIsAdded.surname = surname;
    this.StudentWhoIsAdded.yearOfBirthday = yearOfBirthday;
    this.StudentWhoIsAdded.gender = "M";
    this.StudentWhoIsAdded.description = description;
    this.StudentWhoIsAdded.details = details;
    this.StudentWhoIsAdded.physicalReadiness = physicalReadiness;
    this.StudentWhoIsAdded.behavior = behavior;
    this.StudentWhoIsAdded.responsibility = responsibility;
    this.StudentWhoIsAdded.knowledge = knowledge;
    this.addButton = false;
    this.editButton = false;
    this.store.dispatch(new MyActions.AddStudent(this.StudentWhoIsAdded));
  }

  DeleteStudent(id) {
    this.store.dispatch(new MyActions.DeleteStudent(id));
  }

  improveStudent(student) {
    student.firefighter = true;
    this.store.dispatch(new MyActions.ImproveStudent(student));
  }

  ChangeStudent(student) {
    this.StudentWhoIsAdded.id = student.id;
    this.StudentWhoIsAdded.name = student.name;
    this.StudentWhoIsAdded.surname = student.surname;
    this.StudentWhoIsAdded.yearOfBirthday = student.yearOfBirthday;
    this.StudentWhoIsAdded.gender = student.gender;
    this.StudentWhoIsAdded.description = student.description;
    this.StudentWhoIsAdded.details = student.details;
    this.StudentWhoIsAdded.physicalReadiness = student.physicalReadiness;
    this.StudentWhoIsAdded.behavior = student.behavior;
    this.StudentWhoIsAdded.responsibility = student.responsibility;
    this.StudentWhoIsAdded.knowledge = student.knowledge;
    this.StudentWhoIsAdded.firefighter = student.firefighter;
    this.IWantToEdit();
  }

  EditStudent(name, surname, yearOfBirthday, gender, description, details, physicalReadiness, behavior, responsibility, knowledge) {
    this.StudentWhoIsAdded.name = name;
    this.StudentWhoIsAdded.surname = surname;
    this.StudentWhoIsAdded.yearOfBirthday = yearOfBirthday;
    this.StudentWhoIsAdded.description = description;
    this.StudentWhoIsAdded.details = details;
    this.StudentWhoIsAdded.physicalReadiness = physicalReadiness;
    this.StudentWhoIsAdded.behavior = behavior;
    this.StudentWhoIsAdded.responsibility = responsibility;
    this.StudentWhoIsAdded.knowledge = knowledge;
    this.addButton = false;
    this.editButton = false;
    this.store.dispatch(new MyActions.ImproveStudent(this.StudentWhoIsAdded));
  }
}