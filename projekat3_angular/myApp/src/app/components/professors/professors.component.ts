import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Professor } from '../../models/professor.models';
import { AppState } from '../../app.state';
import { Observable } from 'rxjs';
import * as professorActions from '../../actions/myActions';
import { selectAllProfessors } from '../../reducers/professor.reducer';

@Component({
  selector: 'app-professors',
  templateUrl: './professors.component.html',
  styleUrls: ['./professors.component.css']
})
export class ProfessorsComponent implements OnInit {
  LoginPerson: Observable<Professor>;
  Professors: Professor[];
  constructor(private store: Store<AppState>) {
    this.LoginPerson = store.select(store => store.loginFromReduces.professorWhoLoggedIn);
  }

  ngOnInit() {
    this.getProfessors();
  }

  getProfessors(): void {
    this.store.select(selectAllProfessors).subscribe
      (
        professors => {
          this.Professors = professors
        }
      );
  }

  delete(professorId: string): void {
    this.store.dispatch(new professorActions.DeleteProfessor(professorId));
  }
}