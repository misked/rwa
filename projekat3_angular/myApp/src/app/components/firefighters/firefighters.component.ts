import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Student } from '../../models/student.models';
import { AppState } from '../../app.state';
import { selectAllStudents } from '../../reducers/students.reducer';

@Component({
  selector: 'app-firefighters',
  templateUrl: './firefighters.component.html',
  styleUrls: ['./firefighters.component.css']
})
export class FirefightersComponent implements OnInit {
  FireFighters: Student[];
  constructor(private store: Store<AppState>) {
  }

  ngOnInit() {
    this.getStudents();
  }
  getStudents(): void {
    this.store.select(selectAllStudents).subscribe(
      students => {
        this.FireFighters = students;
      }
    );
  }
}