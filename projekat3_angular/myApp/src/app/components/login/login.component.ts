import { Component, OnInit } from '@angular/core';
import {Store} from '@ngrx/store';
import { Professor } from '../../models/professor.models';
import {AppState} from '../../app.state';
import { Observable } from 'rxjs';
import * as MyActions from '../../actions/myActions';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  professor: Observable<Professor>;
  loading$: Observable<Boolean>;
  error$: Observable<Error>;
  constructor(private store: Store<AppState>) { 
    this.professor=store.select(store=>store.loginFromReduces.professorWhoLoggedIn);
    this.loading$=store.select(store=>store.loginFromReduces.loading);
    this.error$=store.select(store=>store.loginFromReduces.error);
  }

  ngOnInit() {
      //this.store.dispatch(new MyActions.ChangeLogin(this.professor));
  }

  logIn(username,password){
    this.store.dispatch(new MyActions.LoginProfessor(username,password));
  }

}