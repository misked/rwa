import { StudentState } from './reducers/students.reducer';
import { ProfessorState } from './reducers/professor.reducer';
import { LoginState } from './reducers/login.reducer';

export interface AppState {
    readonly studentsFromReducer: StudentState;
    readonly professorsFromReducer: ProfessorState;
    readonly loginFromReduces: LoginState;
}