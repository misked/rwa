export interface Student {
    id: string;
    name: string;
    surname: string;
    yearOfBirthday: number;
    birthday: string;
    gender: string;
    firefighter: boolean;
    description: string;
    details: string;
    physicalReadiness: number;
    behavior: number;
    responsibility: number;
    knowledge: number;
    status: string;
}