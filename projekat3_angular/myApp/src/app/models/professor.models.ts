export interface Professor {
    id: string;
    name: string;
    surname: string;
    gender: string;
    yearOfWork: number;
    bio: string;
    status: string;
    skills: string[];
    country: string;
    email: string;
    password: string;
    loggedIn: Boolean;
}