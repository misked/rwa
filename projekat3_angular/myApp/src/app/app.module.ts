import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule, routingComponents } from './app-routing.module';
import { AppComponent } from './app.component';
import { StoreModule } from '@ngrx/store';
import { studentReducer } from './reducers/students.reducer';
import { professorReducer } from './reducers/professor.reducer';
import { HttpClientModule } from '@angular/common/http';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { EffectsModule } from '@ngrx/effects';
import { SiPEffects } from './effects/SiP.effects';
import { loginReducer } from './reducers/login.reducer';

@NgModule({
  declarations: [
    AppComponent,
    routingComponents
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    StoreModule.forRoot({
      loginFromReduces: loginReducer
    }),
    StoreModule.forFeature('proffesorsFromEntity',professorReducer),
    StoreModule.forFeature('studentsFromEntity',studentReducer),
    EffectsModule.forRoot([SiPEffects]),
    StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: environment.production })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
