import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Student } from '../models/student.models';
import { Professor } from '../models/professor.models';

@Injectable({
  providedIn: 'root'
})
export class PersonsService {

  private STUDENTS_URL = "http://localhost:3000/student";
  private PROFESSORS_URL = "http://localhost:3000/professor";

  constructor(private http: HttpClient) { }

  getStudentsItems() {
    return this.http.get<Student[]>(this.STUDENTS_URL);
  }

  addStudentItem(studentItem: Student) {
    return this.http.post(this.STUDENTS_URL, studentItem);
  }

  deleteStudentItem(id: string) {
    return this.http.delete(`${this.STUDENTS_URL}/${id}`);
  }

  getProfessorsItems() {
    return this.http.get<Professor[]>(this.PROFESSORS_URL);
  }

  addProfessorItem(professorItem: Professor) {
    return this.http.post(this.PROFESSORS_URL, professorItem);
  }

  deleteProfessorItem(id: string) {
    return this.http.delete(`${this.PROFESSORS_URL}/${id}`);
  }

  loginProfessorItem(email: string) {
    return this.http.get<Professor[]>(`${this.PROFESSORS_URL}/?email=${email}`);
  }

  changeLogin(professor: Professor) {
    return this.http.put(`${this.PROFESSORS_URL}/${professor.id}`, professor);
  }

  improveStudentItem(student: Student) {
    return this.http.put(`${this.STUDENTS_URL}/${student.id}`, student);
  }
}
