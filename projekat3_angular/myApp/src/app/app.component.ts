import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Store } from '@ngrx/store';
import { AppState } from './app.state';
import * as MyActions from './actions/myActions';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private httpService: HttpClient, private store: Store<AppState>) {
  }

  ngOnInit() {
    this.store.dispatch(new MyActions.LoadStudent());
    this.store.dispatch(new MyActions.LoadProfessor());
  }
}