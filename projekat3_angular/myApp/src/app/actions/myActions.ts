import { Action } from '@ngrx/store'
import { Student } from '../models/student.models'
import { Professor } from '../models/professor.models'

export const LOAD_STUDENT='[Student] Load Student';
export const LOAD_STUDENT_SUCCESS='[Student] Load Student Success';
export const LOAD_STUDENT_FAILURE='[Student] Load Student Failure';

export const ADD_STUDENT='[Student] Add Student';
export const ADD_STUDENT_SUCCESS='[Student] Add Student Success';
export const ADD_STUDENT_FAILURE='[Student] Add Student Failure';

export const DELETE_STUDENT='[Student] Delete Student';
export const DELETE_STUDENT_SUCCESS='[Student] Delete Student Success';
export const DELETE_STUDENT_FAILURE='[Student] Delete Student Failure';

export const EDIT_STUDENT='[Student] Edit Student';

export const LOAD_PROFESSOR='[Professor] Load Professor';
export const LOAD_PROFESSOR_SUCCESS='[Professor] Load Professor Success';
export const LOAD_PROFESSOR_FAILURE='[Professor] Load Professor Failure';

export const LOGIN_PROFESSOR = '[Professor] Login Professor';
export const LOGIN_PROFESSOR_SUCCESS = '[Professor] Login Professor Success';
export const LOGIN_PROFESSOR_FAILURE = '[Professor] Login Professor Failure';

export const DELETE_PROFESSOR='[Professor] Delete Professor';
export const DELETE_PROFESSOR_SUCCESS='[Professor] Delete Professor Success';
export const DELETE_PROFESSOR_FAILURE='[Professor] Delete Professor Failure';

export const CHANGE_LOGIN = '[Professor] Change Login';

export const IMPROVE_STUDENT = '[Student] Improve Student';
export const IMPROVE_STUDENT_SUCCESS = '[Student] Improve Student Success';
export const IMPROVE_STUDENT_FAILURE = '[Student] Improve Student Failure';


export class LoadStudent implements Action{
    readonly type=LOAD_STUDENT
}

export class LoadStudentSuccess implements Action{
    readonly type=LOAD_STUDENT_SUCCESS
    constructor(public payload: Student[]) {}
}

export class LoadStudentFailure implements Action{
    readonly type=LOAD_STUDENT_FAILURE
    constructor(public payload: Error) {}
}

export class AddStudent implements Action {
    readonly type = ADD_STUDENT
    constructor(public payload: Student) {}
}

export class AddStudentSuccess implements Action {
    readonly type = ADD_STUDENT_SUCCESS
    constructor(public payload: Student) {}
}

export class AddStudentFailure implements Action {
    readonly type = ADD_STUDENT_FAILURE
    constructor(public payload: Error) {}
}

export class DeleteStudent implements Action {
    readonly type = DELETE_STUDENT
    constructor(public payload: string) {}
}

export class DeleteStudentSuccess implements Action {
    readonly type = DELETE_STUDENT_SUCCESS
    constructor(public payload: string) {}
}

export class DeleteStudentFailure implements Action {
    readonly type = DELETE_STUDENT_FAILURE
    constructor(public payload: Error) {}
}

export class EditStudent implements Action {
    readonly type = EDIT_STUDENT
    constructor(public payload: Student) {}
}

export class LoadProfessor implements Action {
    readonly type = LOAD_PROFESSOR
}

export class LoadProfessorSuccess implements Action {
    readonly type = LOAD_PROFESSOR_SUCCESS
    constructor(public payload: Professor[]) {}
}

export class LoadProfessorFailure implements Action {
    readonly type = LOAD_PROFESSOR_FAILURE
    constructor(public payload: Error) {}
}

export class DeleteProfessor implements Action {
    readonly type = DELETE_PROFESSOR
    constructor(public payload: string) {}
}

export class DeleteProfessorSuccess implements Action {
    readonly type = DELETE_PROFESSOR_SUCCESS
    constructor(public payload: string) {}
}

export class DeleteProfessorFailure implements Action {
    readonly type = DELETE_PROFESSOR_FAILURE
    constructor(public payload: Error) {}
}

export class LoginProfessor implements Action {
    readonly type = LOGIN_PROFESSOR
    constructor(public email: string,public password: string) {}
}

export class LoginProfessorSuccess implements Action {
    readonly type = LOGIN_PROFESSOR_SUCCESS
    constructor(public payload: Professor[], public password: string) {
    }
}

export class LoginProfessorFailure implements Action {
    readonly type = LOGIN_PROFESSOR_FAILURE
    constructor(public payload: Error) {}
}

export class ChangeLogin implements Action {
    readonly type=CHANGE_LOGIN
    constructor(public payload: Professor) {}
}

export class ImproveStudent implements Action {
    readonly type = IMPROVE_STUDENT
    constructor(public payload: Student) {}
}

export class ImproveStudentSuccess implements Action {
    readonly type = IMPROVE_STUDENT_SUCCESS
    constructor(public payload: Student) {}
}

export class ImproveStudentFailure implements Action {
    readonly type = IMPROVE_STUDENT_FAILURE
    constructor(public payload: Error) {}
}

export type Actions =LoadStudent 
| LoadStudentSuccess
| LoadStudentFailure
| AddStudent
| AddStudentSuccess
| AddStudentFailure
| DeleteStudent
| DeleteStudentSuccess
| DeleteStudentFailure
| EditStudent 
| LoadProfessor
| LoadProfessorSuccess
| LoadProfessorFailure
| DeleteProfessor
| DeleteProfessorSuccess
| DeleteProfessorFailure
| LoginProfessor
| LoginProfessorSuccess
| LoginProfessorFailure
| ChangeLogin
| ImproveStudent
| ImproveStudentSuccess
| ImproveStudentFailure