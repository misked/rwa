import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import * as MyActions from '../actions/myActions';
import { PersonsService } from '../services/persons.service';
import { catchError, mergeMap, map } from 'rxjs/operators';
import { of } from 'rxjs';


@Injectable()
export class SiPEffects {
    @Effect() loadStudent$ = this.actions$
        .pipe(
            ofType<MyActions.LoadStudent>(MyActions.LOAD_STUDENT),
            mergeMap(
                () => this.personService.getStudentsItems()
                    .pipe(
                        map(data => new MyActions.LoadStudentSuccess(data)),
                        catchError(error => of(new MyActions.LoadStudentFailure(error))
                        )
                    )
            )
        )
    @Effect() loadProfessor$ = this.actions$
        .pipe(
            ofType<MyActions.LoadProfessor>(MyActions.LOAD_PROFESSOR),
            mergeMap(
                () => this.personService.getProfessorsItems()
                    .pipe(
                        map(data => new MyActions.LoadProfessorSuccess(data)),
                        catchError(error => of(new MyActions.LoadProfessorFailure(error))
                        )
                    )
            )
        )

    @Effect() loginProfessor$ = this.actions$
        .pipe(
            ofType<MyActions.LoginProfessor>(MyActions.LOGIN_PROFESSOR),
            mergeMap(
                (dataFromAction) => this.personService.loginProfessorItem(dataFromAction.email)
                    .pipe(
                        map(dataFromServer =>
                            //new MyActions.ChangeLogin(dataFromServer[0]);
                            new MyActions.LoginProfessorSuccess(dataFromServer, dataFromAction.password)),
                        catchError(error => of(new MyActions.LoginProfessorFailure(error))
                        )
                    )
            )
        )

    @Effect() addStudent$ = this.actions$
        .pipe(
            ofType<MyActions.AddStudent>(MyActions.ADD_STUDENT),
            mergeMap(
                (dataFromAction) => this.personService.addStudentItem(dataFromAction.payload)
                    .pipe(
                        map(() => new MyActions.AddStudentSuccess(dataFromAction.payload)),
                        catchError(error => of(new MyActions.AddStudentFailure(error))
                        )
                    )
            )
        )

    @Effect() deleteStudent$ = this.actions$
        .pipe(
            ofType<MyActions.DeleteStudent>(MyActions.DELETE_STUDENT),
            mergeMap(
                (dataFromAction) => this.personService.deleteStudentItem(dataFromAction.payload)
                    .pipe(
                        map(() => new MyActions.DeleteStudentSuccess(dataFromAction.payload)),
                        catchError(error => of(new MyActions.DeleteStudentFailure(error))
                        )
                    )
            )
        )

    @Effect() deleteProfessor$ = this.actions$
        .pipe(
            ofType<MyActions.DeleteProfessor>(MyActions.DELETE_PROFESSOR),
            mergeMap(
                (dataFromAction) => this.personService.deleteProfessorItem(dataFromAction.payload)
                    .pipe(
                        map(() => new MyActions.DeleteProfessorSuccess(dataFromAction.payload)),
                        catchError(error => of(new MyActions.DeleteProfessorFailure(error))
                        )
                    )
            )
        )

    @Effect() improveStudent$ = this.actions$
        .pipe(
            ofType<MyActions.ImproveStudent>(MyActions.IMPROVE_STUDENT),
            mergeMap(
                (dataFromAction) => this.personService.improveStudentItem(dataFromAction.payload)
                    .pipe(
                        map(() => new MyActions.ImproveStudentSuccess(dataFromAction.payload)),
                        catchError(error => of(new MyActions.ImproveStudentFailure(error))
                        )
                    )
            )
        )
    /*@Effect() changeLogin$ = this.actions$
    .pipe(
        ofType<MyActions.ChangeLogin>(MyActions.CHANGE_LOGIN),
        mergeMap(
            (dataFromAction) => this.personService.changeLogin(dataFromAction.payload)
                .pipe(
                    map(dataFromServer =>new MyActions.ChangeLogin(dataFromServer),
                    catchError(error => of(new MyActions.ChangeLogin(error))
                    )
                )
        )
    )*/

    constructor(private actions$: Actions, private personService: PersonsService) {
    }
}