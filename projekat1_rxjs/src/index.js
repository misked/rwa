import { interval, range, Subject, fromEvent, from, timer, zip, of } from "rxjs";
import { take, filter, map, takeUntil, debounceTime, switchMap } from "rxjs/operators"
import { setInterval, clearInterval } from "timers";
import { kockar } from "./kockar";
import { setup } from "./vreme";
import { kolo } from "./kolo";

izvuciIzJson();
nacrtaj();
prvo_kolo();

const subject$=new Subject();
const sub=pokreniNovoKolo(subject$);
var kockar1=new kockar();
var kolo1=new kolo();
var kola1=[], nizod24=[], trenutniNovac=0, brojPogodjenih=0, kvota, brojac, daLiJePrihvatioKombinaciju=false;
const username=document.getElementById("usernameInput");
const loginDugme=document.getElementById("loginDugme");

loginDugme.onclick=(ev)=>{
    document.getElementById("loginDugme").disabled=true;
    diseblujFalse();
}

document.getElementById("dodajPovecanje").onclick=(ev)=>{
    let pomocna=document.getElementById("ulazZaPovecanje").value;
    if(pomocna<=trenutniNovac)
    {
    trenutniNovac-=pomocna;
    document.getElementById("trenutniNovac").innerHTML=trenutniNovac+" rsd.";
    document.getElementById("trenutnoUlozeni").innerHTML=kockar1.novac-trenutniNovac;
    }
    document.getElementById("ulazZaPovecanje").value=0;
}

fromEvent(username, "input")
    .pipe(
        debounceTime(300),
        map(ev => ev.target.value),
        filter(id => id.length > 0),
        switchMap(id => uzmiKockara(id)),
        take(5)
    ).subscribe(kockar => { if(kockar.novac!=undefined)
        {
            if(daLiJePrihvatioKombinaciju==false)
            {popuniZaKockara(kockar);
            loginDugme.disabled=false;
        }
        }
        else {loginDugme.disabled=true;
        disableujTrue();}
    })
    

function izvuciIzJson()
{
    from(fetch("http://localhost:3000/kolo")
    .then(res=> {return res.json()})
    ).subscribe(kola => {kola1=kola; brojac=kola1.length;});
}

function uzmiKockara(id) {
    return from(
        fetch(`http://localhost:3000/kockar/${id}`)
            .then(res => res.json())
    )
}

function popuniZaKockara(kockar)
{
    kockar1=kockar;
    trenutniNovac=kockar1.novac;
    console.log(kockar1);
    var trenutno=document.getElementById("trenutniNovac");
    trenutno.innerHTML=trenutniNovac+" rsd.";
    document.getElementById("uplati").value=0;
}


function nacrtaj()
{
    var divelement=document.getElementById("brojevi");
    nacrtajSestKruga("krugovi");
    nacrtajKombinaciju();
    range(1,48).subscribe( x => {
        let buttonZaBroj=document.createElement("button");
        buttonZaBroj.className="broj";
        buttonZaBroj.id=x;
        buttonZaBroj.disabled=true;
        buttonZaBroj.innerHTML=x;
        divelement.appendChild(buttonZaBroj);
        buttonZaBroj.onclick=(ev) => {
            if(kockar1.kombinacija.length < 6)
            {  
                buttonZaBroj.style.backgroundColor="rgb(40,40,40)";              
                osvezi(x); }
            else if(kockar1.kombinacija.includes(x))
            {
                osvezi(x);
            }
        }
    })
    disableujTrue();
}

function nacrtajSestKruga(ime){
    let divelement=document.getElementById(ime);
    for(let j=1; j<5; j++){
        let labela=document.createElement("label");
        labela.innerHTML="Krug broj :"+j;
        divelement.appendChild(labela);
        let divZaKrug=document.createElement("div");
        divZaKrug.className="krug";

        divelement.appendChild(divZaKrug);
        for( let x=0;x<6;x++)
           {
                let prazno=document.createElement("div");
                prazno.className="krug"+j;
                var string1="rgb("+((Math.random()*1000)%256)+", "+((Math.random()*1000)%256)+", "+((Math.random()*1000)%256)+")";
                prazno.style.backgroundColor=string1;
                divZaKrug.appendChild(prazno);
            }
    }
}

function nacrtajKombinaciju()
{
    let divelement=document.getElementById("kombinacije");
    let labela=document.createElement("label");
    labela.innerHTML="Izaberite kombinaciju,srecno! </br>";
    divelement.appendChild(labela);

    let divZaKrug=document.createElement("div");
    divZaKrug.className="krug";    
    range(1,6).subscribe( x => {
            let prazno=document.createElement("div");
            prazno.className="brojZaKombinaciju";
            var stringic="rgb("+((Math.random()*1000)%256)+", "+((Math.random()*1000)%256)+", "+((Math.random()*1000)%256)+")";
            prazno.style.backgroundColor=stringic;
            divZaKrug.appendChild(prazno);
        })
    divelement.appendChild(divZaKrug);

    let dugme=document.getElementById("prihvatiKombinaciju");
    
    dugme.onclick = (ev) => {
        if(kockar1.kombinacija.length==6)
        {
            var uneto=document.getElementById("uplati").value;
            if(uneto>trenutniNovac || uneto==0)
            {
                alert("Niste uneli korektno uplatu.");
                document.getElementById("uplati").value=0;
            }
            else{
                daLiJePrihvatioKombinaciju=true;
                trenutniNovac-=uneto;
                let trenutno=document.getElementById("trenutniNovac");
                trenutno.innerHTML=trenutniNovac+" rsd.";
                var ulozeni=document.getElementById("trenutnoUlozeni");
                ulozeni.innerHTML=kockar1.novac-trenutniNovac;
                var sviDugmici=document.querySelectorAll(".broj");
                sviDugmici.forEach((br) => {
                    br.disabled=true;
                 })
                dugme.disabled=true;
                document.getElementById("uplati").disabled=true;                
            }
        }
    }
}

function osvezi(izabraniBroj){
    if(!kockar1.kombinacija.includes(izabraniBroj))
    {kockar1.kombinacija.push(izabraniBroj);
    //kockar1.dodajBroj(izabraniBroj);
    }
    else
    {
        kockar1.kombinacija=kockar1.kombinacija.filter((value,index) => {
            document.querySelectorAll(".brojZaKombinaciju")[index].innerHTML="";
            var pom=document.getElementById(izabraniBroj);
            pom.style.backgroundColor="rgb(43, 166, 223)";
            return value!=izabraniBroj;
        })
    }
    crtajKombinaciju();
}

function crtajKombinaciju()
{
    var pom=document.querySelectorAll(".brojZaKombinaciju");
    pom.forEach((krugic,index)=>
        {
            if(kockar1.kombinacija[index]!=undefined)
            krugic.innerHTML=kockar1.kombinacija[index];
        })
}

function izbaciRandomBrojeve() {
    var setIntervalValid=setInterval(function() {
        var kugla=parseInt(((Math.random()*100)%48)+1);
        var index=nizod24.length;
        console.log(kugla);
        if(!nizod24.includes(kugla))
        {
            nizod24.push(kugla);
            if(index<6)
            {
                document.querySelectorAll(".krug1")[index].innerHTML=kugla;
            }
            else if (index<12)
            {
                document.querySelectorAll(".krug2")[index%6].innerHTML=kugla;
            }
            else if (index<18)
            {
                document.querySelectorAll(".krug3")[index%6].innerHTML=kugla;
            }
            else
            {
                document.querySelectorAll(".krug4")[index%6].innerHTML=kugla;
            }
            if((index+1)%6==0)
            {
                sviBrojevi();
                    if(daLiJePrihvatioKombinaciju==true && index<18)
                {
                    setup(10,"vremeZaPovecanjeUloga");
                    document.getElementById("ulazZaPovecanje").disabled=false;
                    document.getElementById("dodajPovecanje").disabled=false;
                    const vreme=timer(10000);
                    vreme.subscribe(y=>{
                        document.getElementById("ulazZaPovecanje").disabled=true;
                    document.getElementById("dodajPovecanje").disabled=true;
                    })
                }
                clearInterval(setIntervalValid);
            }
            proveri(kugla);
        }
    },1000);
}

function proveri(kugla)
{    
    verovatnoca();
    if(kockar1.kombinacija.includes(kugla))
    {
        brojPogodjenih++;
    }
    if(brojPogodjenih==6)
    {
        clearInterval(setIntervalValid);
        alert("CESTITAMO!!Osvojili ste "+(brojPogodjenih*kvota)+ " rsd!");
    }
    else{
        document.getElementById("brojPogodjenih").innerHTML=brojPogodjenih;
    }
    if(nizod24.length==24)
    {
        alert("CESTITAMO!!Osvojili ste "+(brojPogodjenih*kvota)+ " rsd!");
    }
}

function sviBrojevi(){
    let labela=document.getElementById("izvuceniBrojevi");
    const $prvi=of(kockar1.kombinacija);
    const $drugi=of(nizod24);    
    zip($prvi, $drugi).subscribe(x => labela.innerHTML=x);    
}



function prvo_kolo()
{
    uzmiKolo(0);
    setup(100,"vremeRunde");
    setup(20,"vremeZaPovecanjeUloga");
    brojac--;
}

function pokreniNovoKolo(stop$){
    return interval(105000)
    .pipe(takeUntil(stop$),
    map(x=>x+1))
    .subscribe(x=>{
        pocistiSve();
        setup(100,"vremeRunde");
        setup(20,"vremeZaPovecanjeUloga");
        brojac--;
        zaustaviIgru(subject$);
        uzmiKolo(x);});
}

function uzmiKolo(id){
    return from(
        fetch("http://localhost:3000/kolo/" +id)
        .then(res => {return res.json()})
    ).subscribe(kolo => nacrtajZaKolo(kolo));
}

function nacrtajZaKolo(kolo){
    kolo1=kolo;
    let kvotakola=document.getElementById("kvota");
    kvotakola.innerHTML=kolo1.kvota;
    kvota=kolo1.kvota;
    let trenutnoKolo=document.getElementById("trenutnoKolo");
    trenutnoKolo.innerHTML=kolo1.id;
    const source = timer(20000,20000);
    const subscribe=source.subscribe(x=>{
        izbaciRandomBrojeve();
        if(x==3)
        {
            subscribe.unsubscribe();
        }
        if(x==0)
        {
        disableujTrue();
        loginDugme.disabled=true;
        }
        kvota=kvota-x*(kvota/5);
        document.getElementById("kvota").innerHTML=kvota;
    });
}

function zaustaviIgru(stop$){
    if(brojac==1){
        stop$.next();
        stop$.complete();
    }
}


function disableujTrue(){
    document.getElementById("uplati").disabled=true;
    document.getElementById("ulazZaPovecanje").disabled=true;
    document.getElementById("dodajPovecanje").disabled=true;
    document.getElementById("prihvatiKombinaciju").disabled=true;
    range(1,48).subscribe( x => {
        let buttonZaBroj=document.getElementById(x);
        buttonZaBroj.disabled=true;});
}

function diseblujFalse(){
    document.getElementById("uplati").disabled=false;
    document.getElementById("prihvatiKombinaciju").disabled=false;
    range(1,48).subscribe( x => {
        let buttonZaBroj=document.getElementById(x);
        buttonZaBroj.disabled=false;});
}



function verovatnoca(){
    document.getElementById("verovatnoca").innerHTML=(((6-brojPogodjenih)*(24-nizod24.length))/48)+" %";
}

function pocistiSve()
{
    var pom=nizod24.length;
    for(var index=0;index<pom;index++)
        {if(index<6)
            {
                document.querySelectorAll(".krug1")[index].innerHTML="";
            }
            else if (index<12)
            {
                document.querySelectorAll(".krug2")[index%6].innerHTML="";
            }
            else if (index<18)
            {
                document.querySelectorAll(".krug3")[index%6].innerHTML="";
            }
            else
            {
                document.querySelectorAll(".krug4")[index%6].innerHTML="";
            }
        nizod24.pop();
    }
    var trenutno=document.getElementById("trenutniNovac");
    trenutno.innerHTML=0+" rsd.";
    document.getElementById("uplati").value=0;
    document.getElementById("trenutnoUlozeni").value=0;
    document.getElementById("brojPogodjenih").innerHTML=0;
    kvota=0;
    daLiJePrihvatioKombinaciju=false;
    var pom=document.querySelectorAll(".brojZaKombinaciju");
    pom.forEach((krugic,index)=>{
        krugic.innerHTML="";
    })
    username.value="";
    document.getElementById("izvuceniBrojevi").innerHTML="";    
    loginDugme.disabled=false;
    disableujTrue();
    trenutniNovac=0;
    brojPogodjenih=0;
    kockar1=new kockar();
    var verovatnoca=document.getElementById("verovatnoca");
    verovatnoca.innerHTML="";
    range(1,48).subscribe( x => {
        let buttonZaBroj=document.getElementById(x);
        buttonZaBroj.style.backgroundColor="rgb(43, 166, 223)";
    });
}