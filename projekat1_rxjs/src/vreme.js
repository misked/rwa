export function setup(vreme,labela){
    var counter=0;
    var timeleft = vreme;
    var labelaZaVreme=document.getElementById(labela);
    labelaZaVreme.innerHTML=convertSeconds(timeleft-counter);
    var setI=setInterval(function() {
        counter++;
        labelaZaVreme.innerHTML=convertSeconds(timeleft-counter);
        if((timeleft-counter)==0)
        {
            clearInterval(setI);
        }
    }, 1000);
   
}

function convertSeconds(s) {
    var min = pad2(Math.floor(s/60));
    var sec = pad2(s%60);
    return min+ ':' +sec;
}

function pad2(number) {
    return (number < 10 ? '0' : '') + number;
}